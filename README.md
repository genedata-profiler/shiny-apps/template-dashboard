# Template Dashboard
This Template Dashboard provides an example of how to develop [shiny](https://shiny.posit.co/) dashboards with 
[Genedata Profiler](https://www.genedata.com/products-services/profiler). Get up and running by following the 
steps in the [Setup and Run](#setup-and-run) section, and then customize the
dashboard with your own plots and modules following the examples under 
[Dashboard Extension](#how-to-extend-the-dashboard). For an overview of the dashboard user interface and code structure, 
see sections [Dashboard Structure](#dashboard-structure) and [Source Code Structure](#source-code-structure), as well as 
the official documentation of the [Rhino](https://appsilon.github.io/rhino/) framework. 

## Setup and Run

### Setup Project by Cloning Repository
1. Login to Posit Workbench and open a new RStudio Pro session.
2. Create a new project from version control and enter the clone URL of this Template Dashboard project

### Setup Project From Zip-Archive
1. Download a Template Dashboard zip archive from the releases page (zip file should be named `Template-Dashboard_x.y.z_export.zip`).
2. Login to Posit Workbench and open an RStudio Pro session.
3. Create a new project (either in empty directory or from empty Gitlab project in version control)
4. Click `upload` in the RStudio `Files` window. Select the downloaded zip file `Template-Dashboard_x.y.z_export.zip` and upload it to the project target directory.

### Install R Dependencies
If you are using `renv` you can run `renv::init()` to install the neccesary package dependencies. 
Alternatively, you can install the packages mentioned in `dependencies.R` using `install.packages()`. 

### Install Node.js Dependencies
Optionally, you can setup npm to be able to compile new javascript and css code to minified-files. This step requires Node.js and is not necessary if you only want to run the app.
1. In a terminal, change directory into the `.rhino` directory
2. Run the command `npm ci`

### Run App
In an R console you can run the app with `shiny::runApp()`. Alternatively, you can open the `app.R` in RStudio Workbench and  and click the _Run App_ button.

### Build JavaScript or CSS code
To update the JavaScript and/or CSS files open an R session and run `rhino::build_js()` and/or `rhino::build_sass()`. 
See [JavaScript and Sass tools](https://appsilon.github.io/rhino/articles/explanation/node-js-javascript-and-sass-tools.html) in Rhino framework docs for additional information.

## Dashboard Overview
The template dashboard is structured into pages, tabs, and cards, where pages may contain multiple tabs, and tabs may 
contain multiple cards. This structure covers most use cases with only a few restrictions put on the individual 
components, making it possible to opt-out and completely customize the app, see the 
[Dashboard Extension](#how-to-extend-the-dashboard) section.

Pages are the top level component where we recommend to have one main page for the application, and let additional 
pages handling tasks such as configuration, data generation, and administration. To navigate between the pages, use the 
*navbar* which also handles the connection to Profiler. In the `app/main.R` file, we can see a call to the `nav_bar` 
module: `api <- nav_bar$server(NAVBAR, pages, dashboard_config)`, which tetruns a `ProfilerAPI2$ProfilerAPI` object. 
This is the only line that is needed to handel connection and authentication to Profiler. This API object can then be 
passed around and used in the rest of the application. For further information, see `?ProfilerAPI2`. 

A page may consist of multiple *tabs*, together with a sidebar. Here the side-bar is used for fetching and sub-setting 
the data from the Profiler server, and are then processed and plotted in each of the tabs. The data is hence shared 
between the tabs, ensuring minimum number of server calls. For fetching the data, we recommend using the `api$conn` 
obeject which is a `DBI`-instance that can collect tables from Profiler lazily using standard `dbplyr` verbs. E.g. 
`api$conn() %>% tbl('my_table') %>% filter(a > 3) %>% collect()` will filter the table on the server side and only 
fetch the rows where `a > 3`. 

Tabs may contain any shiny module, the template dashboard do however provide another layer of abstraction, the *card*. 
A card encapsulate a set of shiny modules and renders them together. This is particularly useful when a tab has 
multiple plots, or when the plots have associated control components. 

![Image depicting the application layout with pages, tabs, and cards.](img/app-layout.png)

## Source Code Structure
The code is structured around the [rhino](https://appsilon.github.io/rhino/) framework. Rhino uses the R `box` package 
which makes it possible to organize the code into different folders. The code is then seperated into shiny specific 
code (`app/view/`), pure R code (`app/logic/`), javascript code (`app/js/`), and css or sass code (`app/styles/`). The 
folders may then be further structured depending on the use case. The template dashboard, for example, further 
structures the `app/view` folder into pages, tabs, and cards. In addition, the dashboard is making extensive use of the 
`shared/` folder. The shared folder has the same structure as the main app folder, but contains independent and 
reusable components. These components may be used as normal inside the main application and are imported just as other 
modules, e.g. the component `shared/view/layout/page` is imported with `box::use(shared/view/layout/page)`. Other 
notable layouts found in the `shared` folder are the `nav_bar`, `tabs`, `sidebar`, and `card` modules. In the 
`shared/view/components/` folder you can find examples of how to fetch and process data using the api. To add 
javascript to a component see the `shared/view/components/virtual_combo_box` module together with 
the `shared/js/virtual_combo_box` file.

## Dashboard Extension
So far we have shown how to set up a copy of the Template Dashboard project on Posit Workbench, as well as how to run 
the application from there. The next step is then to start customizing the application and create your own dashboard. 
As explained in the [Dashboard Structure](#dashboard-structure) section, the Template Dashboard is structured around 
pages, tabs, and cards. To create one of these layout components, all you have to do is create your own shiny module 
and add it to the corresponding file in the project.
Pages are added in the main file, tabs in one of the page files, and cards in one of the tab files. Below follows three 
example where we have created a new shiny module `app/view/my_module.R` that we would like to add as a page/tab/card. 
```R
# app/view/my_module.R
box::use(
  shiny[NS, verbatimTextOutput, moduleServer, renderText],
  shiny.fluent[Stack, Checkbox.shinyInput]
)

#' export
ui <- function(id) {
  ns <- NS(id)
  Stack(
    padding = 10,
    Checkbox.shinyInput(ns('checkbox'), value = FALSE),
    verbatimTextOutput(ns("text"))
  )
}

#' export
server <- function(id) {
  moduleServer(id, function(input, output, session) {
    output$text <- renderText({ifelse(input$checkbox, 'Is checked!', 'Is not checked')})
  })
}
```

Note that the only assumptions made about the module is that it exports a `ui` and a `server` function. 
Select a toggle to see what alterations need to be made to the corresponding files. In general there are four things 
that need to be added. 

1. Import the module in the file using `box::use`.
2. Define module namespace, title, icon, etc. 
3. Add the `ui` call.
4. Add the `server` call.

<!--- Add Page --->

<details>
  <summary>Add a new page</summary>
<pre>
# app/main.R
box::use(
  shiny,
  shiny[moduleServer, NS,reactive, tags, textOutput, div, reactiveVal, observeEvent, req ],
  shinyjs,
  shinyjs[useShinyjs],
  glue[glue],
  shiny.fluent[fluentPage],
  shiny.router[router_ui, router_server, route, route_link ],
)
<bl>
box::use(
  shared/view/layout/nav_bar,
  shared/logic/utils[read_config, create_logger],
  shared/view/components/fluent_extra[CustomComponentsDefinition, Stack.Item, ThemeProvider, AdaptableCommandBar],
  app/view/pages/page1,
  app/view/pages/page2,
  <mark>app/view/my_module</mark>
)
<bl>
NAVBAR <- 'nav_bar'
LOGGER_APP <- 'app.R'
<bl>
pages <- list(
  page1 = list(
    key = 'page1',
    text = 'Page 1',
    icon_name = 'BIDashboard',
    url_path='/'
  ),
  page2 = list(
    key = 'page2',
    text = 'Page 2',
    icon_name = 'BulletedList',
    url_path='page2'
  ),
  <mark>mypage = list(
    key = 'mypage',
    text = 'My Page',
    icon_name = 'BulletedList',
    url_path='mypage'
  )</mark>
)
<bl>
is_testing <- function() {
  identical(Sys.getenv("TESTTHAT"), "true")
}
<bl>
#' @export
ui <- function(id) {
  ns <- NS(id)
  fluentPage(
    useShinyjs(),
    tags$head(tags$title("Profiler Dashboard")),
    tags$head(tags$link(href = "static/css/profiler-table.css", rel = "stylesheet", type = "text/css")),
    tags$head(CustomComponentsDefinition),
    nav_bar$ui(ns(NAVBAR), nav_items = pages),
    router_ui(
      route(pages$page1$url_path, page1$ui(ns(pages$page1$key))),
      route(pages$page2$url_path, page2$ui(ns(pages$page2$key))),
      <mark>route(pages$mypage$url_path, my_module$ui(ns(pages$mypage$key)))</mark>
    )
  )
}
<bl>
#' @export
server <- function(id) {
  moduleServer(id, function(input, output, session) {
    # framework setup
    router_server()
    log <- create_logger(id)
    log$debug('Initiating server')
<bl>
    config_path <- if (is_testing()) "../../app_config.yml" else "app_config.yml"
    dashboard_config <- read_config(config_path)
<bl>
    api <- nav_bar$server(NAVBAR, pages, dashboard_config)
<bl>
    # pages
    page1$server(pages$page1$key, api, dashboard_config)
    page2$server(pages$page2$key, api, dashboard_config)
    <mark>my_module$server(pages$mypage$key)</mark>
  })
}
</pre>
</details>

<!--- Add Tab --->

<details>
  <summary>Add a new tab</summary>
<pre>
# app/view/pages/page1.R
box::use(
  shiny[div, NS, moduleServer, getQueryString, reactive, req, observeEvent, ],
  purrr[keep, walk],
  glue[glue],
  dplyr[`%>%`, filter, collect, tbl],
  rlang[parse_exprs],
)
<bl>
box::use(
  app/view/tabs/page1_tab1,
  app/view/tabs/page1_tab2,
  shared/view/components/simple_data_input,
  shared/view/layout/tabs,
  shared/view/layout/page[page, ],
  shared/logic/utils[create_logger],
  <mark>app/view/my_module </mark>
)
<bl>
SIDEBAR <- 'sidebar'
HEADER <- 'header'
CONTENT_HEADER <- 'content_header'
CURRENT_TAB_TRIGGER <- 'current_tab'
<bl>
tab_definitions <- function(ns) {
  list(
    page1_tab1 = list(key = 'page1_tab1', label = 'Tab 1', icon_name = 'BarChart4', content_root_id = ns("page1_tab1_content_root")),
    page1_tab2 = list(key = 'page1_tab2', label = 'Tab 2', icon_name = 'AlignHorizontalCenter', content_root_id = ns("page1_tab2_content_root")),
    <mark>mytab = list(key = 'mytab', label = 'My Tab', icon_name = 'AlignHorizontalCenter', content_root_id = ns("mypage_content_root"))</mark>
  )
}
<bl>
.page_content <- function(ns) {
  tabs <- tab_definitions(ns)
  list(
    div(id = tabs$page1_tab1$content_root_id, style = list(display = 'none'), page1_tab1$ui(ns(tabs$page1_tab1$key))),
    div(id = tabs$page1_tab2$content_root_id, style = list(display = 'none'), page1_tab2$ui(ns(tabs$page1_tab2$key))),
    <mark>div(id = tabs$mytab$content_root_id, style = list(display = 'none'), my_module$ui(ns(tabs$mytab$key))) </mark>
  )
}
<bl>
#' @export
ui <- function(id) {
  ns <- NS(id)
  page(
    id = id,
    sidebar_content = simple_data_input$ui(ns(SIDEBAR)),
    content_header = tabs$ui(ns(CONTENT_HEADER)),
    content = .page_content(ns)
  )
}
<bl>
#' @export
server <- function(id, api, config) {
  moduleServer(id, function(input, output, session) {
    ns <- session$ns
    log <- create_logger(id)
<bl>
    data_table <- simple_data_input$server(id = SIDEBAR, api = api, config)
<bl>
    tabs$server(id = CONTENT_HEADER, tab_definitions(ns))
<bl>
    walk(tab_definitions(ns), function(tab) {
      switch(
        tab$key,
        page1_tab1 = page1_tab1$server(tab$key, data_table),
        page1_tab2 = page1_tab2$server(tab$key),
        <mark>mytab = my_module$server(tab$key)</mark>
      )
    })
  })
}
</pre>
</details>

<!--- Add Card --->

<details>
  <summary>Add a new card</summary>
<pre>
# app/view/tabs/page1_tab1.R
box::use(
  shiny[NS, moduleServer, req, renderUI, div, br, uiOutput],
)
<bl>
box::use(
  shared/view/layout/card,
  shared/view/components/table,
  shared/logic/utils,
  app/view/cards/box_plot_card,
  <mark>app/view/my_module</mark>
)
<bl>
TABLE_CARD <- "table_card"
BOX_CARD <- "box_card"
STATS_CARD <- "stats_card"
<mark>MY_CARD <- 'my_card'</mark>
<bl>
#' @export
ui <- function(id) {
  ns <- NS(id)
<bl>
  card_box <- card$make_card(
    box_plot_card$ui(ns(BOX_CARD)),
    title = 'Boxplot'
  )
<bl>
  card_data_table <- card$make_card(
    table$ui(ns(TABLE_CARD)),
    title="Data Table"
  )
<bl>
  card_table_stats <- card$make_card(
    uiOutput(ns(STATS_CARD)),
    style=list(minHeight = "405px"),
    title="Data Statistics"
  )
<bl>
  <mark>my_box <- card$make_card(
    my_module$ui(ns(MY_CARD)),
    title = 'My Card'
  )</mark>
<bl>
  CardRow <- card$CardRow(card_box, card_table_stats)
  CardRow2 <- card$CardRow(<mark>my_box, </mark>card_data_table)
  card$CardPage(CardRow, CardRow2)
}
<bl>
#' @export
server <- function(id, data_table) {
  moduleServer(id, function(input, output, session) {
    ns <- session$ns
    log <- utils$create_logger(id)
<bl>
    box_plot_card$server(BOX_CARD, data_table)
    table$server(TABLE_CARD, data_table, scrollY = 300, scrollX = T, dom = "ftp")
    output[[STATS_CARD]] <- renderUI({
      req(data_table())
      div(
        paste("Number of rows:", nrow(data_table())), br(),
        paste("Number of variables:", ncol(data_table()))
      )
    })
<bl>
    <mark>my_module$server(MY_CARD)</mark>
  })
}
</pre>
</details>


## Additional Information
* [Rhino documentation](https://appsilon.github.io/rhino/)
* [Microsoft Fluent UI for Shiny Apps](https://appsilon.github.io/shiny.fluent/index.html)
* [Official Microsoft Fluent UI](https://developer.microsoft.com/en-us/fluentui#/controls/web)
