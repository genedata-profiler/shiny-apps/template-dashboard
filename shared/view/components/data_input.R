box::use(
  shiny[NS, moduleServer, reactive, observeEvent, ],
  dplyr[`%>%`, ],
  purrr,
  utils,
  glue[glue,],
)

box::use(
  ../layout/sidebar,
  ./message_bar,
  ./data_selector,
  ./data_filter,
  ../../logic/utils[create_logger],
)


SIDEBAR <- "basic_sidebar_id"
DATA_SELECTOR <- "data_selector"
DATA_FILTER <- "data_filter"
SIDEBAR_MESSAGE <- "sidebar_message"

get_config_table_schema <- function (config) {
  config$dashboards %>%
    purrr$map(purrr$pluck, "data") %>%
    purrr$reduce(utils$modifyList) %>%
    purrr$map(~unname(unlist(.x)))
}

#' @export
ui <- function(id) {
  ns <- NS(id)
  # TODO: Add header
  sidebar$ui(
    ns(SIDEBAR),
    # ToDO return the message bar
    data_selector$ui(ns(DATA_SELECTOR)),
    data_filter$ui(ns(DATA_FILTER))
  )
}

#' @export
server <- function(id, api, config) {
  moduleServer(id, function(input, output, session) {
    ns <- session$ns
    log <- create_logger(id)

    sidebar_status <- message_bar$server(SIDEBAR_MESSAGE)

    sidebar$server(SIDEBAR)

    # data source selector
    data_model <- config$data_model
    table_schema <- get_config_table_schema(config)

    view_ids <- data_selector$server(DATA_SELECTOR, api, data_model, table_schema)
    #selected_views <- view_selector$server(VIEW_SELECTOR, api, "view_id")

    filters <- data_filter$server(DATA_FILTER, view_ids, api, config)

    list(
      views = view_ids,
      filters = filters
    )
  })
}
