box::use(
  shiny[moduleServer, observeEvent, NS, div, img, tags, req, isTruthy, is.reactive, reactiveVal, reactive, actionButton, textOutput],
  shiny.react[updateReactInput, reactOutput, renderReact],
  shiny.fluent[Modal, Text, Label, Stack, IconButton, IconButton.shinyInput, PrimaryButton.shinyInput, DialogFooter,
               DefaultButton.shinyInput, updatePrimaryButton.shinyInput, updateCompoundButton.shinyInput,
               CompoundButton.shinyInput],
  shinyjs[useShinyjs],
  htmltools[tagList],
  dplyr[`%>%`, select, arrange, desc, mutate, rename, filter, pull],
  purrr[map_lgl, set_names, map, map2, map_int],
  stringr[str_trunc],
  glue[glue],
  stringr[str_detect],
  readxl[read_excel],
  checkmate[test_string]
)

box::use(
  ./message_bar[status_type],
  ./message_bar,
  ./file_upload,
  ./handsontable,
  ../../logic/utils[create_logger]
)

UPLOAD <- "upload"
TABLE <- "data_table"
MODAL_MESSAGE_BAR <- "modal_message_bar"
SHOW_MODAL_BUTTON <- "showModal"
HIDE_MODAL_BUTTON <- "hideModal"
CONFIRM_MODAL_BUTTON <- "confirmModal"
CANCEL_MODAL_BUTTON <- "cancelModal"


make_modal <- function(ns, show, ..., confirm_disabled = FALSE) {
  Modal(isOpen = show,
        Stack(tokens = list(padding = "15px", childrenGap = "10px"),
              div(style = list(display = "flex"),
                  Text("File Import", variant = "large"),
                  div(style = list(flexGrow = 1)),
                  IconButton.shinyInput(ns(HIDE_MODAL_BUTTON), iconProps = list(iconName = "Cancel")),
              ),
              ...,
              DialogFooter(
                PrimaryButton.shinyInput(ns(CONFIRM_MODAL_BUTTON), text = "Import", disabled = confirm_disabled, style = list(width = "100px")),
                DefaultButton.shinyInput(ns(CANCEL_MODAL_BUTTON), text = "Cancel"), horizontal = TRUE, tokens = list(childrenGap = 10)
              )
        )
  )
}


#' @export
ui <- function(id) {
  ns <- NS(id)
  tagList(
    reactOutput(ns("modal")),
    Label("File Import", htmlFor = ns(SHOW_MODAL_BUTTON), style = list(paddingBottom = "5px")),
    CompoundButton.shinyInput(
      ns(SHOW_MODAL_BUTTON),
      style = list(
        overflow = "hidden",
        display = "block",
        textOverflow = "ellipsis",
        width = "100%",
        marginTop = "0px"
      ),
      text = "Import Excel file ...",
      secondaryText = "",
      disabled = FALSE
    )
  )
}

#' @export
server <- function(id, on_validate, on_import, api) {
  stopifnot(is.reactive(api))

  moduleServer(id, function(input, output, session) {
    ns <- session$ns
    log <- create_logger(id)

    # reactives
    modal_visible <- reactiveVal(FALSE, "modal_visible")
    result_data <- reactiveVal(NULL, "data_frame_res")
    file_data <- reactiveVal(NULL, "file_data")

    upload <- file_upload$server(UPLOAD)

    # read original file content from upload file
    observeEvent(upload(), {
      req(upload())
      log$debug(glue('loading file content for upload {upload()$name}'))
      file_data(read_excel(upload()$datapath))
    })

    current_data <- handsontable$server(TABLE, file_data)
    status <- message_bar$server(MODAL_MESSAGE_BAR)

    # observe changes to data frame as user edits in handsontable
    observeEvent(current_data(), {
      req(current_data())
      log$debug("Trying to validate")

      if (modal_visible()) {
        log$debug("Validating file content")

        updatePrimaryButton.shinyInput(session, inputId = CONFIRM_MODAL_BUTTON, text = "Validating...", disabled = TRUE)
        msg <- on_validate(current_data())

        if (test_string(msg, min.chars = 1)) {
          log$debug(glue("Validation message: {msg}"))
          status$message <- msg
          status$type <- status_type$error
        } else {
          log$debug("Validation OK")
          status$message <- NULL
          status$type <- status_type$success
        }

        updatePrimaryButton.shinyInput(session, inputId = CONFIRM_MODAL_BUTTON, text = "Import", disabled = status$type != status_type$success)
      }
    })

    # observe opening an closing of modal dialog
    observeEvent(input[[SHOW_MODAL_BUTTON]], {
      log$debug('Update status and make file import visible.')
      status$message <- NULL
      status$type <- NULL
      modal_visible(TRUE)
    })

    # hide modal
    observeEvent(c(input[[HIDE_MODAL_BUTTON]], input[[CANCEL_MODAL_BUTTON]]), {
      modal_visible(FALSE)
      file_data(NULL)
    })

    # update result data when modal is confirmed
    observeEvent(input[[CONFIRM_MODAL_BUTTON]], {
      req(current_data(), api()$is_authenticated())
      if (modal_visible()) {
        updatePrimaryButton.shinyInput(session, inputId = CONFIRM_MODAL_BUTTON, text = "Importing...", disabled = TRUE)
        result_data(on_import(current_data(), api()))
        updatePrimaryButton.shinyInput(session, inputId = CONFIRM_MODAL_BUTTON, text = "Import", disabled = FALSE)
        modal_visible(FALSE)
      }
    })

    # show summary of result data source in compound button
    observeEvent(result_data(), {
      log$debug('Updating Select Data source button to result of ds selection.')
      req(result_data())
      ncomp <- result_data()$category_data %>% map_int(~ nrow(.x)) %>% sum()
      ncat <- length(result_data()$category_data)
      secondary_text <- glue("{ncomp} compounds in {ncat} categories", ncomp = ncomp, ncat = ncat)
      updateCompoundButton.shinyInput(session, inputId = SHOW_MODAL_BUTTON, text = upload()$name,
                                      secondaryText = secondary_text, disabled = FALSE)
    })

    # render the modal into output
    output$modal <- renderReact({
      log$debug('Render file import modal')
      make_modal(
        ns = ns,
        show = modal_visible(),
        file_upload$ui(ns(UPLOAD)),
        handsontable$ui(ns(TABLE), width = "1200px", height = "600px"),
        message_bar$ui(ns(MODAL_MESSAGE_BAR))
      )
    })

    reactive(result_data())
  })
}
