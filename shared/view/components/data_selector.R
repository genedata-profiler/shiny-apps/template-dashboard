box::use(
  shiny[moduleServer, observeEvent, NS, div, img, tags, req, isTruthy, is.reactive, reactiveVal, reactive, bindCache, getUrlHash, updateQueryString, actionButton, ],
  shiny.react[updateReactInput, reactOutput, renderReact],
  shiny.fluent[Modal, Text, Stack, IconButton, IconButton.shinyInput, PrimaryButton.shinyInput, DialogFooter,
               DefaultButton.shinyInput, updatePrimaryButton.shinyInput, updateCompoundButton.shinyInput,
               CompoundButton.shinyInput],
  shinyjs[useShinyjs],
  htmltools[tagList],
  dplyr[`%>%`, select, arrange, desc, mutate, rename, filter, pull],
  purrr[map_lgl, set_names, map, map2],
  purrr,
  stringr[str_trunc],
  glue[glue],
  stringr[str_detect],
)

box::use(
  ./message_bar[status_type],
  ./message_bar,
  ./table,
  ../utils/router[parse_url_hash, create_query_param_reactive],
  ../../logic/utils[create_logger]
)

TABLE <- "data_selector_table"
MODAL_MESSAGE_BAR <- "modal_message_bar"
SHOW_MODAL_BUTTON <- "showModal"
HIDE_MODAL_BUTTON <- "hideModal"
CONFIRM_MODAL_BUTTON <- "confirmModal"
CANCEL_MODAL_BUTTON <- "cancelModal"

.validate_data_source <- function(ds, status) {
  invalid <- map_lgl(ds$views, ~!isTruthy(.x))
  if (any(invalid)) {
    invalid_domains <- paste(names(ds$views)[which(invalid)], collapse = ", ")
    status$message <- glue("Unable to infer tables for {domains} in data source {ds$name}", domains = invalid_domains)
    status$type <- status_type$error

  } else {
    all_domains <- paste(names(ds$views), collapse = ", ")
    status$message <- glue("Successfully inferred tables for {domains} in data source {ds$name}", domains = all_domains)
    status$type <- status_type$success
  }
}

.make_modal <- function(ns, show, ..., confirm_disabled = FALSE) {
  Modal(isOpen = show,
        Stack(tokens = list(padding = "15px", childrenGap = "10px"),
              div(style = list(display = "flex"),
                  Text("Select Data Source", variant = "large"),
                  div(style = list(flexGrow = 1)),
                  IconButton.shinyInput(ns(HIDE_MODAL_BUTTON), iconProps = list(iconName = "Cancel")),
              ),
              ...,
              DialogFooter(
                PrimaryButton.shinyInput(ns(CONFIRM_MODAL_BUTTON), text = "Select", disabled = confirm_disabled, style = list(width = "32px")),
                DefaultButton.shinyInput(ns(CANCEL_MODAL_BUTTON), text = "Cancel"), horizontal = TRUE, tokens = list(childrenGap = 10)
              )
        )
  )
}

.fetch_data_model_views <- function(data_model, api) {
  api$analytics_views$list(metadata = list("Data Model" = data_model)) %>%
    select(id, studyName, created, creator, rowCount, columnCount, description) %>%
    arrange(desc(created)) %>%
    mutate(created = format(created, format = "%Y-%m-%d %H:%M")) %>%
    rename(
      "View ID" = id,
      "Study" = studyName,
      "Created" = created,
      "Creator" = creator,
      "Rows" = rowCount,
      "Columns" = columnCount,
      "Description" = description
    )
}

.get_view_id <- function(views, attribute, value) {
  views %>%
    filter(.data[[attribute]] == value) %>%
    pull(viewId)
}

.infer_data_source <- function(view_id, table_schema, status, api) {
  view <- tryCatch(api$analytics_views$details(view_id),
                   error = function(e) {
                     status$type <- status_type$error
                     status$message <- 'The view_id specified in the url does not exist in Genedata Profiler or you do not have the
      permission to access it.'
                     return(NULL)
                   }
  )

  if (is.null(view)) {
    return(NULL)
  }

  # data source name
  ds_name <- view$metadata$`Data Source`
  if (is.null(ds_name)) {
    ds_name <- paste(view$studyName, view$path, sep = "\n")
  }

  ds_view_ids <- tryCatch({
    # infer other data source tables in same folder
    folder_id <- api$data_lake$details(view$objectId)$parent
    ds_views <- api$data_lake$list(folder_id, type = "view", all_attributes = TRUE)
    ds_view_ids <- names(table_schema) %>%
      purrr$set_names() %>%
      purrr$map(~.get_view_id(ds_views, "Data Type", .x))

  },
    error = function(e) {
      status$type <- status_type$error
      status$message <- sprintf('Please make sure that the metadata attribute \'Data Type\' is set for all views in parent folder \'%s\' of view \'%s\'.', folder_id, view_id)
      return(NULL)
    }
  )

  if (is.null(ds_view_ids)) {
    return(NULL)
  }

  ds <- as.data_source(list(
    name = ds_name,
    views = ds_view_ids
  ))
  .validate_data_source(ds, status)
  return(ds)
}

as.data_source <- function(ds) {
  stopifnot(is.list(ds))
  structure(ds, class = "data_source")
}


#' @export
ui <- function(id) {
  ns <- NS(id)
  tagList(
    reactOutput(ns("modal")),
    Text(variant = "large", "Data Source"),
    CompoundButton.shinyInput(
      ns(SHOW_MODAL_BUTTON),
      style = list(
        overflow = "hidden",
        display = "block",
        textOverflow = "ellipsis",
        width = "100%"
      ),
      text = "Loading dataset views ...",
      secondaryText = "",
      disabled = FALSE
    )
  )
}

#' @export
server <- function(id, api, data_model, table_schema, view_url_param = 'view_id') {
  stopifnot(is.reactive(api))
  stopifnot(is.character(data_model))
  stopifnot(is.list(table_schema))

  moduleServer(id, function(input, output, session) {
    ns <- session$ns
    log <- create_logger(id)

    # reactives
    modal_visible <- reactiveVal(FALSE, "modal_visible")
    data_source <- reactiveVal(NULL, "data_source")
    data_source_res <- reactiveVal(NULL, "data_source_res")
    view_id <- reactiveVal(NULL, "view_id")
    view_id_res <- create_query_param_reactive(view_url_param)

    views <- reactive({
      req(api()$is_authenticated())
      log$debug(glue('querying views with data model {data_model}'))
      .fetch_data_model_views(data_model, api())
    }, label = "views") %>% bindCache("views", cache = "session")

    table_selection <- table$server(TABLE, views, scrollY = 500)

    status <- message_bar$server(MODAL_MESSAGE_BAR)

    # observers for opening an closing modal dialog
    observeEvent(input[[SHOW_MODAL_BUTTON]], {
      log$debug('Update status and make data selector visible.')
      status$message <- NULL
      status$type <- NULL
      modal_visible(TRUE)
    })

    observeEvent(c(input[[HIDE_MODAL_BUTTON]], input[[CANCEL_MODAL_BUTTON]], input[[CONFIRM_MODAL_BUTTON]]), {
      modal_visible(FALSE)
    })

    # update result data source when modal is confirmed
    observeEvent(input[[CONFIRM_MODAL_BUTTON]], {
      req(data_source())
      view_id_res(view_id())
      data_source_res(data_source())
    })

    # auto-infer from top-most view
    observeEvent(views(), {
      req(views(), length(views()) >= 1)
      if (is.null(view_id_res()) || !view_id_res() %in% views()$`View ID`) {
        view_id_res(views()[1,]$`View ID`)
      }
      if (isTruthy(views())){
        log$debug('Updating Select Data source button to select.')
        updateCompoundButton.shinyInput(session, inputId = SHOW_MODAL_BUTTON, text = "Please click here to select a data source", disabled = FALSE)
      }
    })

    # update selected view ID when user selects a table row
    observeEvent(table_selection(), {
      req(views(), length(table_selection()) == 1)
      view_id(views()[table_selection(),]$`View ID`)
    })

    # update selected view ID when set from outside via URL
    observeEvent(view_id_res(), {
      if (is.null(view_id_res()) && is.null(view_id())) {
        req(views())
        view_id_res(views()[1,]$`View ID`)
      } else if (is.null(view_id_res())) {
        view_id_res(view_id())
      } else {
        view_id(view_id_res())
      }
    }, ignoreNULL = FALSE)

    # try auto-infer data source when user selects a table row
    observeEvent({ view_id(); table_selection(); views() }, {
      req(view_id(), views())
      log$debug(glue("Inferring data source from view id '{view_id()}'."))
      if (modal_visible()) {
        # Infer data source from table selection
        # Infer data source from table selection
        updatePrimaryButton.shinyInput(session, inputId = CONFIRM_MODAL_BUTTON, text = "Wait...", disabled = TRUE)
        log$debug('Updating data source with visible modal window')
        new_ds <- .infer_data_source(view_id(), table_schema, status, api())
        data_source(new_ds)
        updatePrimaryButton.shinyInput(session, inputId = CONFIRM_MODAL_BUTTON, text = "Select", disabled = status$type != status_type$success)
      } else {
        # Infer data source triggered programmatically via view_id()
        log$debug('Updating data source with invisible modal window')
        new_ds <- .infer_data_source(view_id(), table_schema, status, api())
        if (status$type == status_type$success) {
          data_source_res(new_ds)
        }
      }
    })


    # show summary of result data source in compound button
    observeEvent(data_source_res(), {
      req(data_source_res())
      secondary_text <- data_source_res()$views %>%
        str_trunc(width = 45, side = "center") %>%
        paste(collapse = "\n")
      log$debug('Updating Select Data source button to result of ds selection.')
      updateCompoundButton.shinyInput(session, inputId = SHOW_MODAL_BUTTON, text = data_source_res()$name,
                                      secondaryText = secondary_text, disabled = FALSE)
    })

    # render the modal into output
    output$modal <- renderReact({
      log$debug('Render Data Selector Modal')
      .make_modal(
        ns = ns,
        show = modal_visible(),
        confirm_disabled = status$type != status_type$success,
        table$ui(ns(TABLE), width = "1200px"),
        message_bar$ui(ns(MODAL_MESSAGE_BAR))
      )
    })

    reactive(data_source_res()$views)

  })
}
