box::use(
  shiny[div],
  shiny.fluent[Stack],
  glue[glue]
)

box::use(
  ../ui/theme[main_theme],
  ../components/fluent_extra[ThemeProvider, Stack.Item]
)

page_styles <- list(
  root = list(
    display = 'grid',
    gridTemplateColumns = 'auto 1fr',
    gridTemplateRows = 'auto auto 1fr',
    gridTemplateAreas = '
        "page-header page-header"
        "sidebar content-header"
        "sidebar content"
      ',
    backgroundColor = "#faf9f8",
    # TODO: substract potential horizontal scrollbar
    height = 'calc(100vh - 55px)'
  ),
  sidebar = list(
    gridArea = 'sidebar'
  ),
  pageHeader = list(
    gridArea = 'page-header'
  ),
  contentHeader = list(
    gridArea = 'content-header',
    height = '40px',
    padding = 8
  ),
  content = list(
    gridArea = 'content',
    minHeight = 0,
    padding = '15px',
    overflowY = 'auto'
  )
)

page_header_styles <- list(
  root = list(
    backgroundColor = 'white',
    borderBottom = glue("1px solid {main_theme$palette$neutralLight}")
  ),

  content = list(
    display = 'block',
    padding = '10px 15px'
  )
)

make_page <- function(id, content, content_header = NULL, sidebar_content = NULL, page_header = NULL) {
  root_children <- list(div(style = page_styles$content, content))

  if (!is.null(content_header)) {
    root_children <- c(list(div(style = page_styles$contentHeader, content_header)), root_children)
  }

  if (!is.null(sidebar_content)) {
    root_children <- c(list(div(style = page_styles$sidebar, sidebar_content)),
                       root_children)
  }

  if (!is.null(page_header)) {
    root_children <- c(list(
      div(style = page_styles$pageHeader,
          Stack(styles = page_header_styles, Stack.Item(grow = TRUE, style = page_header_styles$content, page_header))
      )
    ), root_children)
  }

  div(id = id, style = page_styles$root, root_children)
}

#' @export
page <- function(id, content, content_header = NULL, sidebar_content = NULL, page_header = NULL) {
  ThemeProvider(
    theme = main_theme,
    make_page(id, content, content_header, sidebar_content, page_header)
  )
}
