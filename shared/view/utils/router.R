box::use(
  shiny[getDefaultReactiveDomain, reactive, req, getUrlHash, isTruthy, getQueryString, reactiveVal, observeEvent,updateQueryString],
  shiny.router[parse_url_path, ],
  purrr[map2,],
  glue[glue,]
)

parse_query_to_string <- function(query) {
  if (is.null(query)) {
    return('')
  }
  query_string <- paste(map2(names(query), query, ~paste0(.x, '=', .y)), collapse = '&')
  if (query_string != '') {
    query_string <- paste0('?', query_string)
  }
  query_string
}

parse_url_hash <- function(url_hash) {
  if (is.null(url_hash) || url_hash == '') {
    return(list(path = '/', query = NULL))
  }
  url_parts <- parse_url_path(url_hash)
  if (url_parts$path == '') {
    url_parts$path <- '/'
  }
  url_parts
}

create_path_reactive <- function(session = getDefaultReactiveDomain()) {
  rv <- reactiveVal(NULL)

  observeEvent(getUrlHash(), {
    url_parts <- parse_url_hash(getUrlHash())
    if (isTruthy(url_parts$path)) {
      rv(url_parts$path)
    }
  })

  rv
}

create_query_param_reactive <- function(param_name, observe_once = TRUE, session = getDefaultReactiveDomain()) {
  stopifnot(isTruthy(param_name))
  rv <- reactiveVal(NULL)

  observeEvent(getUrlHash(), {
    parsed_url <- parse_url_hash(getUrlHash())
    query <- parsed_url$query
    if (!identical(rv(), query[[param_name]]))  {
      rv(query[[param_name]])
    }
  }, once = observe_once)

  observeEvent(rv(), {
    parsed_url <- parse_url_hash(getUrlHash())
    query <- parsed_url$query
    path <- parsed_url$path
    if (path == '') {
      path <- '/'
    }

    if (!identical(rv(), query[[param_name]])) {
      query[[param_name]] <- rv()
      query_string <- parse_query_to_string(query)
      url_hash <- glue("#!{path}{qs}", path = path, qs = query_string)
      updateQueryString(url_hash, mode = "push")
    }
  }, ignoreNULL = FALSE)

  rv
}
