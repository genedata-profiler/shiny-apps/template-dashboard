// Util methods and extensions
Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

function ensureArray(x) {
    if (!(typeof (x) === 'object' && x.length)) {
        return [x];
    }
    return x;
}

// VirtualComboBox selection state management
var selectionState = {};

export function updateComboBoxSelection(id, event, option) {
    if ("selected" in option) {
        if (option.selected) {
            selectionState[id].push(option.key)
        } else {
            selectionState[id].remove(option.key)
        }
    } else {
        selectionState[id] = []
        selectionState[id].push(option.key)
    }
    Shiny.setInputValue(id, selectionState[id]);
}

function initComboBoxSelection(message) {
    if (message.defaultSelected) {
        selectionState[message.id] = ensureArray(message.defaultSelected)
    } else {
        selectionState[message.id] = []
    }
    Shiny.setInputValue(message.id, selectionState[message.id]);
}

$(function() {
    Shiny.addCustomMessageHandler('virtual-combo-box-init', initComboBoxSelection);
});

export function updateDashboardURL(key) {
    let searchParams = new URLSearchParams(window.location.search)
    searchParams.set("page", key);
    const newRelativePathQuery = window.location.pathname + '?' + searchParams.toString();
    window.history.replaceState({}, 'Profiler Dashboard', newRelativePathQuery);
}

