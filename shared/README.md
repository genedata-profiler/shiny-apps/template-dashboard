# Shared

## How to include 'Shared' project in your dashboard
* Add Shared as a submodule `git submodule add  https://gitlab-pro.ch.genedata.com/off-core/shiny-apps/shared.git shared`
* Update the `.rhino/webpack.config.js` by adding a second entry, e.g.:
```
const appDir = join(__dirname, '..', 'app');
const sharedDir = join(__dirname, '..', 'shared');

module.exports = {
  mode: 'production',
  entry: [join(appDir, 'js', 'index.js'), join(sharedDir, 'js', 'index.js')],
  <etc>
```
* In the `.gitmodule` file, change the path to be relative to the current project.
* Add `GIT_SUBMODULE_STRATEGY: recursive` to the variables section of the current projects `.gitlab-ci.yml`.

## How to update
You can update your dashboard as usual. When you `pull` the `shared` submodule will also be updated. You can make changes to the `shared` directory too, when these are pushed, it is not the current project that is updated, but instead the shared project. That also means that you will need to push a second time, to update the commit hash of the `shared` submodule that is beeing used.

